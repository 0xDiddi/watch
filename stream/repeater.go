package stream

import "io"

type repeater struct {
	underlying ResettableStreamer
	times      int
}

func (r *repeater) Read(b []byte) (int, error) {
makeThisBetter:
	n, err := r.underlying.Read(b)
	if err != nil {
		if err == io.EOF {
			if r.times > 0 {
				r.times--
				if r.times == 0 {
					return n, err
				}
			}
			r.underlying.Reset()
			// if we reached EOF and nothing was read
			// ie we were at the very end, try again.
			// todo: make this a loop or something
			if n == 0 {
				goto makeThisBetter
			}
		} else {
			return n, err
		}
	}
	return n, nil
}

func Repeat(s ResettableStreamer, n int) Streamer {
	return &repeater{
		underlying: s,
		times:      n,
	}
}

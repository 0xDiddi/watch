package main

import (
	"bufio"
	"bytes"
	"flag"
	"fmt"
	"github.com/faiface/beep"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
	"github.com/lmittmann/ppm"
	"gitlab.com/0xDiddi/watch/stream"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"time"
)

func main() {
	argSrc := flag.String("i", "input.mp4", "The input file.")
	argWidth := flag.Int("w", 64, "The output width.")
	argHeight := flag.Int("h", 36, "The output height.")
	argRate := flag.Int("r", 15, "The framerate.")
	argAudio := flag.Bool("audio", false, "Enable audio.")
	argDouble := flag.Bool("dp", false, "Double each pixel to compensate for rectangle-ness of characters.")
	// argRepeat := flag.Int("repeat", 1, "How often to repeat. Negative for infinite.")

	flag.Parse()

	videoBuffer := bytes.NewBuffer(nil)
	audioBuffer := bytes.NewBuffer(nil)

	ffmpeg := exec.Command("ffmpeg",
		"-i", *argSrc, // input file
		"-map", "0:v", // video output...
		"-r", strconv.Itoa(*argRate), // framerate
		"-s", fmt.Sprintf("%dx%d", *argWidth, *argHeight), // resolution
		"-c:v", "ppm", // ppm encoding
		"-f", "rawvideo", // raw format
		"pipe:1") // output to stdout
	ffmpeg.Stdout = videoBuffer

	if *argAudio {
		ffmpeg.Args = append(ffmpeg.Args,
			"-map", "0:a", // audio output...
			"-c:a", "libmp3lame", // convert to mp3 encoding, just to be sure
			"-f", "mp3", // mp3 format
			"pipe:2") // output to stderr
		ffmpeg.Stderr = audioBuffer
	}

	//noinspection GoUnhandledErrorResult
	err := ffmpeg.Start()
	if err != nil {
		panic(err)
	}

	// bufReader :=
	videoStreamer := stream.FromReader(bufio.NewReader(videoBuffer))
	var audioStreamer beep.StreamSeekCloser
	if *argAudio {
		for audioBuffer.Len() < 65536 {
		}
		var format beep.Format
		audioStreamer, format, err = mp3.Decode(ioutil.NopCloser(audioBuffer))
		if err != nil {
			fmt.Printf("Couldn't decode audio stream. Make sure the video has audio.\n")
			return
		}

		err = speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))
		if err != nil {
			fmt.Printf("Couldn't init speaker: %+v\n", err)
			return
		}
	}

	frameDelta := time.Second / time.Duration(*argRate)
	lastFrame := time.Now()

	// 13 bytes header, 3 bytes per pixel
	// todo: header contains size in ascii, so adjust header size
	//  based on specified size.
	frameSize := 13 + (*argWidth)*(*argHeight)*3
	bufOut := bufio.NewWriterSize(os.Stdout, frameSize)

	// wait until ffmpeg starts outputting data
	// otherwise the first read fails with EOF
	// this way, it'll wait until we read enough
	for videoBuffer.Len() < frameSize {
	}

	audioChan := make(chan bool)
	if *argAudio {
		go func() {
			<-audioChan
			speaker.Play(audioStreamer)
		}()
	}
	_, _ = fmt.Fprint(bufOut, "\033[H\033[J")
	for {
		imgStr, err := stream.Take(videoStreamer, frameSize)
		if err != nil {
			break
		}
		img, err := ppm.Decode(imgStr)
		if err != nil {
			fmt.Printf("error during decoding:\n%+v\n", err)
			return
		}

		lastFrame = time.Now()
		_, _ = fmt.Fprint(bufOut, "\033[H")
		for y := 0; y < *argHeight; y++ {
			for x := 0; x < *argWidth; x++ {
				r, g, b, _ := img.At(x, y).RGBA()
				if *argDouble {
					pixel := fmt.Sprintf("\x1b[38;2;%d;%d;%dm█\x1b[0m", r, g, b)
					_, _ = fmt.Fprint(bufOut, pixel, pixel)
				} else {
					_, _ = fmt.Fprintf(bufOut, "\x1b[38;2;%d;%d;%dm█\x1b[0m", r, g, b)
				}
			}
			_, _ = fmt.Fprint(bufOut, "\n")
		}

		select {
		case audioChan <- true:
		default:
		}

		_ = bufOut.Flush()
		<-time.After((frameDelta - (time.Since(lastFrame))) / 2)
	}
}
